await import("./src/env.mjs")
import i18nConfig from "./next-i18next.config.cjs"

/** @type {import("next").NextConfig} */
const config = {
  reactStrictMode: true,
  i18n: i18nConfig.i18n,
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "flagsapi.com",
        port: "",
        pathname: "/**"
      }
    ]
  }
}

export default config
