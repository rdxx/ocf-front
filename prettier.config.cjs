/** @type {import("prettier").Config} */
const config = {
  trailingComma: "none",
  semi: false
}

module.exports = config
