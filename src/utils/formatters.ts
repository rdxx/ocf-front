import {
  type Course,
  type Section,
  type Chapter,
  type ApiCourse,
  type User,
  type ApiSection,
  type ApiUser
} from "@/types/courses"

export const apiCourseToCourse = (apiCourse: ApiCourse): Course => ({
  id: apiCourse.ID,
  name: apiCourse.Name,
  theme: apiCourse.Theme,
  format: apiCourse.Format,
  fileName: apiCourse.CourseID_str + ".html",
  author: {
    id: apiCourse.owner.id,
    email: apiCourse.owner.email,
    firstName: apiCourse.owner.firstname,
    lastName: apiCourse.owner.lastname
  },
  title: apiCourse.Title,
  subtitle: apiCourse.Subtitle,
  header: apiCourse.Header,
  footer: apiCourse.Footer,
  logo: apiCourse.Logo,
  description: apiCourse.Description,
  schedule: apiCourse.Schedule,
  prelude: apiCourse.Prelude,
  learningObjectives: apiCourse.learning_objectives,
  chapters: apiCourse.chapters.map((chapter) => ({
    id: chapter.ID,
    createdAt: new Date(chapter.CreatedAt),
    updatedAt: new Date(chapter.UpdatedAt),
    deletedAt: chapter.DeletedAt ? new Date(chapter.DeletedAt) : null,
    title: chapter.Title,
    number: chapter.Number,
    footer: chapter.Footer,
    introduction: chapter.Introduction,
    sections: chapter.sections.map(apiSectionToSection)
  })) satisfies Chapter[]
})

export const apiSectionToSection = (apiSection: ApiSection): Section =>
  ({
    id: apiSection.ID,
    createdAt: new Date(apiSection.CreatedAt),
    updatedAt: new Date(apiSection.UpdatedAt),
    deletedAt: apiSection.DeletedAt ? new Date(apiSection.DeletedAt) : null,
    title: apiSection.Title,
    number: apiSection.Number,
    introduction: apiSection.Intro,
    conclusion: apiSection.Conclusion,
    fileName: apiSection.FileName
  } satisfies Section)

export const apiUserToUser = (apiUser: ApiUser): User =>
  ({
    id: apiUser.id,
    email: apiUser.email,
    firstName: apiUser.firstname,
    lastName: apiUser.lastname
  } satisfies User)

export const getUserFullName = (user: User) =>
  user.firstName + " " + user.lastName

export const getUserInitials = (user: User) =>
  String(user.firstName[0]).toUpperCase() +
  String(user.lastName[0]).toUpperCase()
