import { env } from "@/env.mjs"

export const config = {
  name: "OCF Front",
  links: {
    gitlab: "https://gitlab.com/rdxx/ocf-front",
    api: env.NEXT_PUBLIC_API_URL,
    apiPrefix: env.NEXT_PUBLIC_API_PREFIX
  },
  localStorage: {
    tokenKey: "ocf-token",
    refreshKey: "ocf-refresh-token"
  }
}
