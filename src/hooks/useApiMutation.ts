import axios, { type AxiosRequestConfig } from "axios"
import { useMutation } from "@tanstack/react-query"
import { useAuth } from "@/providers/AuthProvider"
import { config } from "@/utils/config"

const useApiMutation = <T, K>(
  method: "post" | "put" | "delete",
  path: string,
  options: AxiosRequestConfig = {}
) => {
  const { token: jwt } = useAuth()
  const opts: AxiosRequestConfig = {
    baseURL: config.links.api + "/" + config.links.apiPrefix,
    ...options
  }

  const mutation = useMutation<K, unknown, T>({
    mutationFn: async (data) => {
      const token = "bearer " + (jwt ?? "")

      if (jwt) {
        if (!opts.headers) opts.headers = {}

        opts.headers.Authorization = token
      }

      const response = await axios[method]<K>(
        path,
        ["get", "delete"].includes(method) ? opts : data,
        opts
      )

      return response.data
    }
  })

  return mutation
}

export default useApiMutation
