import axios, { type AxiosRequestConfig } from "axios"
import {
  useQuery,
  type UseQueryOptions,
  type QueryKey
} from "@tanstack/react-query"
import { config } from "@/utils/config"
import { useAuth } from "@/providers/AuthProvider"

const useApiQuery = <T, K>(
  path: string,
  queryOptions: Omit<
    UseQueryOptions<K, unknown, T, QueryKey>,
    "queryFn" | "refetchOnWindowFocus"
  > = {},
  axiosOptions: AxiosRequestConfig = {},
  isFile = false
) => {
  const { token: jwt } = useAuth()
  const opts: AxiosRequestConfig = {
    baseURL: config.links.api + (isFile ? "" : "/" + config.links.apiPrefix),
    ...axiosOptions
  }

  const query = useQuery<K, unknown, T>({
    queryFn: async () => {
      const token = "bearer " + (jwt ?? "")

      if (jwt) {
        if (!opts.headers) opts.headers = {}

        opts.headers.Authorization = token
      }

      const response = await axios.get<K>(path, opts)
      return response.data
    },
    refetchOnWindowFocus: false,
    ...queryOptions
  })

  return query
}

export default useApiQuery
