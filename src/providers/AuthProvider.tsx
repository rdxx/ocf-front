"use client"

import { config } from "@/utils/config"
import { useRouter } from "next/router"
import { createContext, useContext, useEffect, useState } from "react"

type Props = {
  children: React.ReactNode
}

type AuthContextType = {
  token?: string | null
  storeTokens: (token: string, refreshToken: string) => void
}

const publicPaths = ["/sign-in"]

const AuthContext = createContext<AuthContextType | null>(null)
const AuthProvider = ({ children }: Props) => {
  const [token, setToken] = useState<string | null>(null)
  const [authenticated, setAuthenticated] = useState(false)
  const router = useRouter()
  const allowedPath = publicPaths.includes(router.pathname)

  useEffect(() => {
    if (!token) {
      const localToken = localStorage.getItem(config.localStorage.tokenKey)
      const localRefreshToken = localStorage.getItem(
        config.localStorage.refreshKey
      )

      if (localToken && localRefreshToken) {
        setToken(localToken)
        setAuthenticated(true)
      } else {
        if (allowedPath) return

        setAuthenticated(false)

        void router.replace("/sign-in")
        return
      }
    }
  }, [allowedPath, router, token])

  const storeTokens = (token: string, refreshToken: string) => {
    localStorage.setItem(config.localStorage.tokenKey, token)
    localStorage.setItem(config.localStorage.refreshKey, refreshToken)

    setToken(token)
    setAuthenticated(true)
  }

  return (
    <AuthContext.Provider value={{ token, storeTokens }}>
      {allowedPath ? children : authenticated && children}
    </AuthContext.Provider>
  )
}

export const useAuth = () => useContext(AuthContext) as AuthContextType
export default AuthProvider
