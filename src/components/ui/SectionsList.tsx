import ContentDisplayCard from "./ContentDisplayCard"
import { type Section } from "@/types/courses"

type Props = {
  sections: Section[]
}

const SectionsList = ({ sections }: Props) => {
  console.log(sections)

  return (
    <div className="ml-6 md:ml-14 flex flex-col my-2 gap-2">
      {sections.map((section) => (
        <ContentDisplayCard
          key={section.id}
          href={"/sections/" + section.id}
          prefix={section.number}
          title={section.title}
          description={section.introduction}
          compact
        />
      ))}
    </div>
  )
}

export default SectionsList
