import { PresentationIcon } from "lucide-react"
import { type Course } from "@/types/courses"
import UserDetails from "./UserDetails"
import Image from "next/image"

type Props = {
  course: Course
}

const CourseDetails = ({ course }: Props) => {
  return (
    <div className="flex flex-col w-full md:flex-row gap-8">
      <Image
        className="rounded-md"
        src={course.logo || "/fallback_logo.png"}
        width={500}
        height={300}
        alt="course logo"
      />
      <div className="flex flex-col justify-between w-full gap-y-6">
        <div className="flex gap-2 flex-col ">
          <h1 className="text-3xl font-semibold">{course.title}</h1>
          <p className="text-muted-foreground text-md">{course.description}</p>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2">
          <UserDetails user={course.author} />
          <div className="flex items-center justify-end">
            <a
              target="_blank"
              href={course.id + "/presentation"}
              rel="noopener noreferrer"
            >
              <PresentationIcon className="w-6 h-6 text-primary hover:text-primary/70" />
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CourseDetails
