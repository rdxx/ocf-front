import MarkdownPreview from "./MarkdownPreview"
import AdvancedEditor from "./AdvancedEditor"
import StarterKit from "@tiptap/starter-kit"
import { Markdown } from "tiptap-markdown"
import { useEditor } from "@tiptap/react"
import { useState } from "react"

const LiveEditor = () => {
  const [content, setContent] = useState("")
  const [preview, setPreview] = useState(true)

  const editor = useEditor({
    extensions: [StarterKit, Markdown],
    content,
    onUpdate: ({ editor }) => {
      setContent(editor.getHTML())
    },
    editorProps: {
      attributes: {
        class: "focus-visible:outline-none"
      }
    }
  })

  return (
    <div className="flex flex-col md:flex-row gap-4 min-h-[500px]">
      <AdvancedEditor
        editor={editor}
        preview={preview}
        onPreviewToggle={() => setPreview(!preview)}
      />
      <MarkdownPreview content={content} preview={preview} />
    </div>
  )
}

export default LiveEditor
