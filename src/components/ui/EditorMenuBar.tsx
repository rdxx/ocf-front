import { buttonVariants } from "../generics/Button"
import { type Editor } from "@tiptap/react"
import { Toggle } from "../generics/Toggle"
import cn from "@/utils/cn"
import {
  BoldIcon,
  Code2Icon,
  ItalicIcon,
  PanelLeftCloseIcon,
  PanelRightCloseIcon,
  StrikethroughIcon
} from "lucide-react"

export type PreviewProps = {
  preview?: boolean
  onPreviewToggle?: () => void
}

type Props = {
  editor: Editor | null
} & PreviewProps

const EditorMenuBar = ({ editor, preview, onPreviewToggle }: Props) => {
  if (!editor) return null

  const items = [
    {
      icon: BoldIcon,
      onClick: editor.commands.toggleBold
    },
    {
      icon: ItalicIcon,
      onClick: editor.commands.toggleItalic
    },
    {
      icon: StrikethroughIcon,
      onClick: editor.commands.toggleStrike
    },
    {
      icon: Code2Icon,
      onClick: editor.commands.toggleCode
    }
  ]
  const SidebarIcon = preview ? PanelRightCloseIcon : PanelLeftCloseIcon

  return (
    <div className="border border-border p-1 rounded-t-xl space-x-1 flex justify-between">
      <div>
        {items.map(({ icon: Icon, onClick }, i) => (
          <Toggle key={i} className="rounded-lg" onClick={onClick}>
            <Icon className="h-4 w-4" />
          </Toggle>
        ))}
      </div>
      <div
        className={cn(
          buttonVariants({
            variant: "ghost"
          }),
          "w-10 px-0 hover:cursor-pointer"
        )}
        onClick={onPreviewToggle}
      >
        <SidebarIcon className="h-4 w-4" />
        <span className="sr-only">GitLab</span>
      </div>
    </div>
  )
}

export default EditorMenuBar
