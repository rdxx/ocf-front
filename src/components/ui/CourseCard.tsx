import { type Course } from "@/types/courses"
import Image from "next/image"

type Props = {
  course: Course
}

const CourseCard = ({ course }: Props) => {
  return (
    <div className="flex flex-col w-[400px] h-[250px]">
      <Image
        className="object-cover rounded-t-xl"
        width={400}
        height={250}
        src={course.logo || "/fallback_logo.png"}
        alt="course logo"
      />
      <div className="rounded-b-sm bg-primary/20 p-2 backdrop-blur-md h-[50px]">
        <h3 className="mx-2 truncate">{course.title}</h3>
      </div>
    </div>
  )
}

export default CourseCard
