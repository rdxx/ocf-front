import { type User } from "@/types/courses"
import { Avatar, AvatarFallback } from "../generics/Avatar"
import { getUserFullName, getUserInitials } from "@/utils/formatters"

type Props = {
  user: User
}

const UserDetails = ({ user }: Props) => {
  const initials = getUserInitials(user)
  const fullName = getUserFullName(user)

  return (
    <div className="flex gap-4 items-center">
      <Avatar>
        <AvatarFallback>{initials}</AvatarFallback>
      </Avatar>
      <p>{fullName}</p>
    </div>
  )
}

export default UserDetails
