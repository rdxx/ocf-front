import { EditorContent, type Editor } from "@tiptap/react"
import EditorMenuBar, { type PreviewProps } from "./EditorMenuBar"
import cn from "@/utils/cn"

type Props = {
  editor: Editor | null
} & PreviewProps

const AdvancedEditor = ({ editor, ...rest }: Props) => {
  return (
    <div className={cn("w-full h-full", { "w-1/2": rest.preview })}>
      <EditorMenuBar editor={editor} {...rest} />
      <EditorContent
        className="bg-accent rounded-b-xl p-4 min-h-[500px]"
        editor={editor}
      />
    </div>
  )
}

export default AdvancedEditor
