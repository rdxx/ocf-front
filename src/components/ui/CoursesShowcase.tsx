import { type ApiCourse, type Course } from "@/types/courses"
import { apiCourseToCourse } from "@/utils/formatters"
import useApiQuery from "@/hooks/useApiQuery"
import Skeleton from "../generics/Skeleton"
import CourseCard from "./CourseCard"
import Link from "next/link"

const CoursesShowcase = () => {
  const { data, isFetching } = useApiQuery<Course[], ApiCourse[]>("courses/", {
    initialData: [],
    select: (data) => data.map(apiCourseToCourse)
  })

  if (!data || isFetching) return <Skeleton className="w-[300px] h-[200px]" />

  return (
    <div className="flex gap-6">
      {data.map((course) => (
        <Link
          key={course.id}
          href={"/courses/" + course.id}
          className="hover:cursor-pointer hover:opacity-90"
        >
          <CourseCard key={course.id} course={course} />
        </Link>
      ))}
    </div>
  )
}

export default CoursesShowcase
