import ReactMarkdown from "react-markdown"
import rehypeRaw from "rehype-raw"
import { type PreviewProps } from "./EditorMenuBar"

type Props = {
  content: string
} & Pick<PreviewProps, "preview">

const MarkdownPreview = ({ content, preview }: Props) => {
  if (!preview) return null

  return (
    <ReactMarkdown
      className="w-1/2 bg-accent/70 rounded-xl p-4"
      rehypePlugins={[rehypeRaw]}
    >
      {content}
    </ReactMarkdown>
  )
}

export default MarkdownPreview
