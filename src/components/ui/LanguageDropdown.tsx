import { useTranslation } from "next-i18next"
import {
  DropdownMenu,
  DropdownMenuItem,
  DropdownMenuTrigger,
  DropdownMenuContent
} from "@/components/generics/DropdownMenu"
import { Button } from "../generics/Button"
import Image from "next/image"
import { useMemo } from "react"
import { useRouter } from "next/router"

const availableLanguages = [
  {
    label: "Français",
    value: "fr",
    flag: "https://flagsapi.com/FR/flat/64.png"
  },
  {
    label: "English",
    value: "en",
    flag: "https://flagsapi.com/US/flat/64.png"
  }
]

const LanguageDropdown = () => {
  const router = useRouter()
  const {
    i18n: { language }
  } = useTranslation()

  const currentLanguageItem = useMemo(
    () =>
      availableLanguages.find(
        (availableLanguage) => availableLanguage.value === language
      ),
    [language]
  )

  const handleLanguageChange = (language: string) => {
    void router.push(
      {
        pathname: router.pathname,
        query: router.query
      },
      router.asPath,
      { locale: language }
    )
  }

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant="icon" size="icon">
          <Image
            className="opacity-80"
            src={currentLanguageItem?.flag ?? ""}
            alt={currentLanguageItem?.label ?? "flag"}
            width={32}
            height={16}
          />
          <span className="sr-only">Change language</span>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        {availableLanguages.map((availableLanguage) => (
          <DropdownMenuItem
            key={availableLanguage.value}
            onClick={() => handleLanguageChange(availableLanguage.value)}
            className="gap-2"
          >
            <Image
              className="opacity-80"
              src={availableLanguage.flag}
              alt={availableLanguage.label}
              width={28}
              height={20}
            />
            <span>{availableLanguage.label}</span>
          </DropdownMenuItem>
        ))}
      </DropdownMenuContent>
    </DropdownMenu>
  )
}

export default LanguageDropdown
