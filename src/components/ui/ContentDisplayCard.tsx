import cn from "@/utils/cn"
import Link from "next/link"

type Props = {
  href: string
  prefix: string | number
  title: string
  description?: string
  compact?: boolean
}

const ContentDisplayCard = ({
  href,
  prefix,
  title,
  description,
  compact
}: Props) => {
  return (
    <Link
      className="w-full bg-accent flex gap-4 p-2 px-3 rounded-md hover:bg-accent/60 hover:cursor-pointer items-center"
      href={href}
    >
      <span>{prefix}</span>
      <div
        className={cn("flex", {
          "flex-col": !compact,
          "gap-2 items-center": compact
        })}
      >
        <h3>{title}</h3>
        {description && !compact && (
          <span className="text-sm text-muted-foreground">{description}</span>
        )}
        {description && compact && (
          <div>
            <span className="text-muted-foreground text-sm">
              — {description}
            </span>
          </div>
        )}
      </div>
    </Link>
  )
}

export default ContentDisplayCard
