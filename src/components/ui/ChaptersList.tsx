import ContentDisplayCard from "./ContentDisplayCard"
import { type Chapter } from "@/types/courses"
import { useTranslation } from "next-i18next"
import SectionsList from "./SectionsList"

type Props = {
  chapters: Chapter[]
}

const ChaptersList = ({ chapters }: Props) => {
  const { t } = useTranslation("courses")

  return (
    <div className="flex flex-col gap-4">
      <h2 className="text-2xl font-semibold">{t("chapters.title")}</h2>
      <div className="space-y-6">
        {chapters
          .sort((a, b) => a.number - b.number)
          .map((chapter) => (
            <div key={chapter.id} className="">
              <ContentDisplayCard
                href={`/chapters/${chapter.id}`}
                prefix={chapter.number}
                title={chapter.title}
                description={chapter.introduction}
              />
              <SectionsList sections={chapter.sections} />
            </div>
          ))}
      </div>
    </div>
  )
}

export default ChaptersList
