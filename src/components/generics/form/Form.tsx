import enZodTranslation from "zod-i18n-map/locales/en/zod.json"
import frZodTranslation from "public/locales/fr/zod.json"
import { useTranslation } from "next-i18next"
import { makeZodI18nMap } from "zod-i18n-map"
import { useEffect } from "react"
import i18next from "i18next"
import clsx from "clsx"
import { z } from "zod"
import {
  Formik,
  type FormikValues,
  type FormikConfig,
  Form as FormikForm
} from "formik"

type Props<T> = {
  className?: string
  children: React.ReactNode
} & FormikConfig<T>

const Form = <T extends FormikValues>({
  className,
  children,
  ...rest
}: Props<T>) => {
  const {
    i18n: { language }
  } = useTranslation()

  useEffect(() => {
    const initZodI18n = async () => {
      await i18next.init({
        lng: language,
        resources: {
          en: {
            zod: enZodTranslation
          },
          fr: {
            zod: frZodTranslation
          }
        }
      })

      z.setErrorMap(makeZodI18nMap({ t: i18next.t }))
    }

    void initZodI18n()
  }, [language])

  return (
    <Formik<T> {...rest}>
      <FormikForm noValidate className={clsx("flex flex-col gap-4", className)}>
        {children}
      </FormikForm>
    </Formik>
  )
}

export default Form
