import Input, { type InputProps } from "../Input"
import { useField } from "formik"
import Label from "../Label"

type Props = {
  name: string
  label: string
} & InputProps

const InputFormField = ({ name, label, ...rest }: Props) => {
  const [field, { error, touched, initialTouched }] = useField({ name })
  const shoudDisplayError = touched && error && !initialTouched

  return (
    <div className="w-full">
      <Label htmlFor={name}>{label}</Label>
      <Input id={name} {...field} {...rest} />
      {shoudDisplayError && <Label htmlFor={name}>{error}</Label>}
    </div>
  )
}

export default InputFormField
