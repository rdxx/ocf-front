import { useFormikContext } from "formik"
import { Button } from "../generics/Button"

type Props = {
  label: string
}

const SubmitButton = ({ label }: Props) => {
  const { isValid, dirty } = useFormikContext()
  const shouldDisable = !(isValid && dirty)

  return (
    <Button type="submit" disabled={shouldDisable}>
      {label}
    </Button>
  )
}

export default SubmitButton
