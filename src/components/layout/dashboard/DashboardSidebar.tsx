import { useMemo, useRef, useState } from "react"
import { usePathname } from "next/navigation"
import { useTranslation } from "next-i18next"
import { useWindowSize } from "usehooks-ts"
import Link from "next/link"
import cn from "@/utils/cn"
import {
  Tooltip,
  TooltipTrigger,
  TooltipContent,
  TooltipProvider
} from "@/components/generics/Tooltip"
import {
  UserIcon,
  BuildingIcon,
  FingerprintIcon,
  PresentationIcon,
  ChevronsRightIcon
} from "lucide-react"

const DashboardSidebar = () => {
  const { t } = useTranslation(["courses", "organisations", "users", "roles"])
  const pathname = usePathname()
  const [expanded, setExpanded] = useState(false)
  const ref = useRef<HTMLDivElement>(null)
  const { width } = useWindowSize()

  const sidebarSize = useMemo(() => {
    return width * 0.047562 < 60 ? 60 : width * 0.047562
  }, [width])

  const items = [
    {
      label: t("courses:title"),
      icon: PresentationIcon,
      href: "/dashboard/courses"
    },
    {
      label: t("organisations:title"),
      icon: BuildingIcon,
      href: "/dashboard/organisations"
    },
    {
      label: t("users:title"),
      icon: UserIcon,
      href: "/dashboard/users"
    },
    {
      label: t("roles:title"),
      icon: FingerprintIcon,
      href: "/dashboard/roles"
    }
  ]

  return (
    <div
      ref={ref}
      className={cn(
        "hidden w-[5%] min-w-[60px] transition-all duration-150 ease-in-out sm:flex sm:flex-row-reverse",
        { "w-[20%] min-w-[20%]": expanded }
      )}
    >
      <div
        className={cn(
          "peer fixed top-1/2 flex h-6 w-6 cursor-pointer items-center justify-center rounded-full bg-border transition-all duration-150 ease-in-out hover:bg-neutral-500",
          { "rotate-180": expanded }
        )}
        onClick={() => setExpanded(!expanded)}
        style={{
          left: `calc(${expanded ? "20%" : String(sidebarSize) + "px"} - 13px)`
        }}
      >
        <ChevronsRightIcon className="h-4 w-4 text-primary" />
      </div>
      <div className="w-full border-r border-border transition-all duration-150 ease-in-out peer-hover:border-r-2 peer-hover:border-r-neutral-700">
        <div className="flex flex-col">
          {items.map((item, i) => (
            <Link
              key={item.label}
              href={item.href}
              className={cn(
                "group m-2 mt-0 flex rounded-md p-2 text-primary/70 transition-all duration-150 ease-out hover:bg-accent hover:text-primary hover:ease-in",
                {
                  "bg-accent text-primary": pathname === item.href,
                  "justify-center": !expanded,
                  "mt-2": i === 0
                }
              )}
            >
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger>
                    <div className="flex h-[1.5rem] max-h-[1.5rem] items-center gap-4">
                      <item.icon className={cn("h-5 w-5")} />
                      <span className={cn("hidden", { block: expanded })}>
                        {item.label}
                      </span>
                    </div>
                  </TooltipTrigger>
                  <TooltipContent dir="right">
                    <span className="text-xs">{item.label}</span>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
            </Link>
          ))}
        </div>
      </div>
    </div>
  )
}

export default DashboardSidebar
