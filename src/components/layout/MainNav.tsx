"use client"

import { usePathname } from "next/navigation"
import { useTranslation } from "next-i18next"
import { config } from "@/utils/config"
import Link from "next/link"
import cn from "@/utils/cn"

const MainNav = () => {
  const { t } = useTranslation("common")
  const pathname = usePathname()

  const items = [
    {
      label: t("dashboard"),
      path: "/dashboard"
    }
  ]

  return (
    <div className="mr-4 hidden md:flex">
      <Link href="/" className="mr-6 flex items-center space-x-2">
        <span className="hidden font-bold sm:inline-block">{config.name}</span>
      </Link>
      <nav className="flex items-center space-x-6 text-sm font-medium">
        {items.map((item) => (
          <Link
            key={item.label}
            href={item.path}
            className={cn(
              "hover:text-foreground/80 transition-colors",
              pathname === item.path ? "text-foreground" : "text-foreground/60"
            )}
          >
            {item.label}
          </Link>
        ))}
      </nav>
    </div>
  )
}

export default MainNav
