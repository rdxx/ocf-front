import DashboardSidebar from "@/components/layout/dashboard/DashboardSidebar"
import Header from "@/components/layout/Header"
import cn from "@/utils/cn"

type Props = {
  title?: string
  header?: boolean
  sidebar?: boolean
  center?: boolean
  children: React.ReactNode
}

const Page = ({ title, header = true, sidebar, center, children }: Props) => {
  return (
    <div className={cn("flex min-h-screen flex-col")}>
      {header && <Header />}
      <main className={cn("flex grow", { "flex-row": sidebar })}>
        {sidebar && <DashboardSidebar />}
        <div
          className={cn(
            "flex w-full flex-col gap-4 rounded-md container py-8",
            {
              "w-full max-w-[600px] items-center justify-center": center
            }
          )}
        >
          {title && <span className="text-4xl font-medium">— {title}</span>}
          {children}
        </div>
      </main>
    </div>
  )
}

export default Page
