import LanguageDropdown from "@/components/ui/LanguageDropdown"
import { buttonVariants } from "@/components/generics/Button"
import ThemeToggle from "@/components/ui/ThemeToggle"
import MainNav from "@/components/layout/MainNav"
import { GitlabIcon } from "lucide-react"
import { config } from "@/utils/config"
import Link from "next/link"
import cn from "@/utils/cn"

const Header = () => {
  return (
    <header className="supports-backdrop-blur:bg-background/60 sticky top-0 z-40 w-full border-b bg-background/95 backdrop-blur">
      <div className="container flex h-14 items-center">
        <MainNav />
        {/* <MobileNav /> */}
        <div className="flex flex-1 items-center justify-between space-x-2 md:justify-end">
          {/* <div className="w-full flex-1 md:w-auto md:flex-none">
            <CommandMenu />
          </div> */}
          <nav className="flex items-center">
            <Link href={config.links.gitlab} target="_blank" rel="noreferrer">
              <div
                className={cn(
                  buttonVariants({
                    variant: "ghost"
                  }),
                  "w-10 px-0"
                )}
              >
                <GitlabIcon className="h-4 w-4 fill-current" />
                <span className="sr-only">GitLab</span>
              </div>
            </Link>
            <LanguageDropdown />
            <ThemeToggle />
          </nav>
        </div>
      </div>
    </header>
  )
}

export default Header
