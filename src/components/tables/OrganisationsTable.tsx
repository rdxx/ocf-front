import AddOrganisationDialog from "../dialogs/AddOrganisationDialog"
import { type Organisation } from "@/types/organisations"
import { type ColumnDef } from "@tanstack/react-table"
import { useTranslation } from "next-i18next"
import DataTable from "../generics/DataTable"
import useApiQuery from "@/hooks/useApiQuery"

export const OrganisationsTable = () => {
  const { t } = useTranslation("fields")
  const { data, isFetching } = useApiQuery<Organisation[], Organisation[]>(
    "organisations/",
    { initialData: [], select: (data) => (String(data) === "null" ? [] : data) }
  )

  const columns: ColumnDef<Organisation>[] = [
    { accessorKey: "id", header: t("id") },
    { accessorKey: "name", header: t("name") }
  ]

  if (isFetching || !data) return <p>Loading...</p>

  return (
    <div>
      <div className="flex justify-end py-4">
        <AddOrganisationDialog />
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  )
}
