import AddCourseFromGitDialog from "../dialogs/AddCourseFromGitDialog"
import { type ApiCourse, type Course } from "@/types/courses"
import { apiCourseToCourse } from "@/utils/formatters"
import { type Row, type ColumnDef } from "@tanstack/react-table"
import { MoreHorizontalIcon } from "lucide-react"
import { useTranslation } from "next-i18next"
import DataTable from "../generics/DataTable"
import useApiQuery from "@/hooks/useApiQuery"
import { Button } from "../generics/Button"
import {
  DropdownMenu,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuTrigger,
  DropdownMenuContent,
  DropdownMenuSeparator
} from "../generics/DropdownMenu"
import useApiMutation from "@/hooks/useApiMutation"
import { useCallback } from "react"

const CoursesTable = () => {
  const { t } = useTranslation("fields")
  const { data, isFetching } = useApiQuery<Course[], ApiCourse[]>("courses/", {
    select: (data) => data.map(apiCourseToCourse)
  })
  const generateMutation = useApiMutation("post", "courses/generate")

  const handleGeneration = useCallback(
    (row: Row<Course>) => {
      const getGenerationInput = (format: "html" | "markdown") => ({
        authorEmail: row.original.author.email,
        format,
        name: row.original.name,
        theme: row.original.theme
      })

      console.log(getGenerationInput("html"))

      generateMutation.mutate(getGenerationInput("html"))
      generateMutation.mutate(getGenerationInput("markdown"))
    },
    [generateMutation]
  )

  const columns: ColumnDef<Course>[] = [
    { accessorKey: "id", header: t("id") },
    { accessorKey: "title", header: t("title") },
    { accessorKey: "description", header: t("description") },
    { accessorKey: "theme", header: t("theme") },
    {
      id: "actions",
      enableHiding: false,
      cell: ({ row }) => (
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button variant="ghost" className="h-8 w-8 p-0">
              <span className="sr-only">Open menu</span>
              <MoreHorizontalIcon className="h-4 w-4" />
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
            <DropdownMenuLabel>Actions</DropdownMenuLabel>
            <DropdownMenuSeparator />
            <DropdownMenuItem onClick={() => handleGeneration(row)}>
              Generate media
            </DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
      )
    }
  ]

  if (isFetching || !data) return <p>Loading...</p>

  return (
    <div>
      <div className="flex justify-end py-4">
        <AddCourseFromGitDialog />
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  )
}

export default CoursesTable
