import { type ColumnDef } from "@tanstack/react-table"
import AddRoleDialog from "../dialogs/AddRoleDialog"
import { useTranslation } from "next-i18next"
import DataTable from "../generics/DataTable"
import useApiQuery from "@/hooks/useApiQuery"
import { type Role } from "@/types/roles"

export const RolesTable = () => {
  const { t } = useTranslation("fields")
  const { data, isFetching } = useApiQuery<Role[], Role[]>("roles/", {
    initialData: [],
    select: (data) => (String(data) === "null" ? [] : data)
  })

  const columns: ColumnDef<Role>[] = [
    { accessorKey: "id", header: t("id") },
    { accessorKey: "roleName", header: t("name") }
  ]

  if (isFetching || !data) return <p>Loading...</p>

  return (
    <div>
      <div className="flex justify-end py-4">
        <AddRoleDialog />
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  )
}
