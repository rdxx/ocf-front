import { type ApiUser, type User } from "@/types/courses"
import { type ColumnDef } from "@tanstack/react-table"
import AddUserDialog from "../dialogs/AddUserDialog"
import { apiUserToUser } from "@/utils/formatters"
import { useTranslation } from "next-i18next"
import DataTable from "../generics/DataTable"
import useApiQuery from "@/hooks/useApiQuery"

export const UsersTable = () => {
  const { t } = useTranslation("fields")
  const { data, isFetching } = useApiQuery<User[], ApiUser[]>("users/", {
    select: (data) => data.map(apiUserToUser)
  })

  const columns: ColumnDef<User>[] = [
    { accessorKey: "id", header: t("id") },
    { accessorKey: "email", header: t("email") },
    { accessorKey: "firstName", header: t("firstName") },
    { accessorKey: "lastName", header: t("lastName") }
  ]

  if (isFetching || !data) return <p>Loading...</p>

  console.log(data)

  return (
    <div>
      <div className="flex justify-end py-4">
        <AddUserDialog />
      </div>
      <DataTable columns={columns} data={data} />
    </div>
  )
}
