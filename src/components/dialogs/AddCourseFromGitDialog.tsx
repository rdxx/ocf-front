import { toFormikValidationSchema } from "zod-formik-adapter"
import InputFormField from "../generics/form/InputFormField"
import SubmitButton from "../buttons/SubmitButton"
import useApiMutation from "@/hooks/useApiMutation"
import { type ApiCourse } from "@/types/courses"
import { useTranslation } from "next-i18next"
import { Button } from "../generics/Button"
import Form from "../generics/form/Form"
import { useState } from "react"
import { z } from "zod"
import {
  Dialog,
  DialogTitle,
  DialogHeader,
  DialogFooter,
  DialogTrigger,
  DialogContent,
  DialogDescription
} from "../generics/Dialog"

type AddCourseFromGitValues = z.infer<typeof validationSchema>

const validationSchema = z.object({
  url: z.string().nonempty()
})

const initialValues = {
  url: ""
}

const AddCourseFromGitDialog = () => {
  const { t } = useTranslation(["fields", "courses"])
  const { mutate } = useApiMutation<AddCourseFromGitValues, ApiCourse>(
    "post",
    "courses/git"
  )
  const [open, setOpen] = useState(false)

  const onSubmit = (values: AddCourseFromGitValues) => {
    mutate(values, {
      onSuccess: () => {
        setOpen(false)
      }
    })
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button variant="secondary">{t("courses:addGit.title")}</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <Form<AddCourseFromGitValues>
          validationSchema={toFormikValidationSchema(validationSchema)}
          initialValues={initialValues}
          onSubmit={onSubmit}
          className="w-full flex flex-col gap-6"
        >
          <DialogHeader>
            <DialogTitle>{t("courses:addGit.title")}</DialogTitle>
            <DialogDescription>
              {t("courses:addGit.description")}
            </DialogDescription>
          </DialogHeader>
          <div className="grid gap-4 py-4">
            <div className="flex items-center gap-4">
              <InputFormField label={t("fields:url")} name="url" />
            </div>
          </div>
          <DialogFooter>
            <SubmitButton label={t("fields:add")} />
          </DialogFooter>
        </Form>
      </DialogContent>
    </Dialog>
  )
}

export default AddCourseFromGitDialog
