import { toFormikValidationSchema } from "zod-formik-adapter"
import InputFormField from "../generics/form/InputFormField"
import { type Organisation } from "@/types/organisations"
import SubmitButton from "../buttons/SubmitButton"
import useApiMutation from "@/hooks/useApiMutation"
import { useTranslation } from "next-i18next"
import { Button } from "../generics/Button"
import Form from "../generics/form/Form"
import { z } from "zod"
import {
  Dialog,
  DialogTitle,
  DialogHeader,
  DialogFooter,
  DialogTrigger,
  DialogContent
} from "../generics/Dialog"
import { useState } from "react"

type AddOrganisationValues = z.infer<typeof validationSchema>

const validationSchema = z.object({
  name: z.string().nonempty()
})

const initialValues = {
  name: ""
}

const AddOrganisationDialog = () => {
  const { t } = useTranslation(["fields", "organisations"])
  const { mutate } = useApiMutation<AddOrganisationValues, Organisation>(
    "post",
    "organisations/"
  )
  const [open, setOpen] = useState(false)

  const onSubmit = (values: AddOrganisationValues) => {
    mutate(values, {
      onSuccess: () => {
        setOpen(false)
      }
    })
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button variant="secondary">{t("organisations:add.title")}</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <Form<AddOrganisationValues>
          validationSchema={toFormikValidationSchema(validationSchema)}
          initialValues={initialValues}
          onSubmit={onSubmit}
          className="w-full flex flex-col gap-6"
        >
          <DialogHeader>
            <DialogTitle>{t("organisations:add.title")}</DialogTitle>
          </DialogHeader>
          <div className="grid gap-4 py-4">
            <div className="flex items-center gap-4">
              <InputFormField label={t("fields:name")} name="name" />
              {/* <Label htmlFor="name" className="text-right">
              Name
              </Label>
              <Input id="name" value="Pedro Duarte" className="col-span-3" />
              </div>
              <div className="grid grid-cols-4 items-center gap-4">
              <Label htmlFor="username" className="text-right">
              Username
              </Label>
            <Input id="username" value="@peduarte" className="col-span-3" /> */}
            </div>
          </div>
          <DialogFooter>
            <SubmitButton label={t("fields:add")} />
          </DialogFooter>
        </Form>
      </DialogContent>
    </Dialog>
  )
}

export default AddOrganisationDialog
