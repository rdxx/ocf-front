import { toFormikValidationSchema } from "zod-formik-adapter"
import InputFormField from "../generics/form/InputFormField"
import SubmitButton from "../buttons/SubmitButton"
import useApiMutation from "@/hooks/useApiMutation"
import { useTranslation } from "next-i18next"
import { Button } from "../generics/Button"
import { type Role } from "@/types/roles"
import Form from "../generics/form/Form"
import { useState } from "react"
import { z } from "zod"
import {
  Dialog,
  DialogTitle,
  DialogHeader,
  DialogFooter,
  DialogTrigger,
  DialogContent
} from "../generics/Dialog"

type AddRoleValues = z.infer<typeof validationSchema>

const validationSchema = z.object({
  roleName: z.string().nonempty()
})

const initialValues = {
  roleName: ""
}

const AddRoleDialog = () => {
  const { t } = useTranslation(["fields", "roles"])
  const { mutate } = useApiMutation<AddRoleValues, Role>("post", "roles/")
  const [open, setOpen] = useState(false)

  const onSubmit = (values: AddRoleValues) => {
    mutate(values, {
      onSuccess: () => {
        setOpen(false)
      }
    })
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button variant="secondary">{t("roles:add.title")}</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <Form<AddRoleValues>
          validationSchema={toFormikValidationSchema(validationSchema)}
          initialValues={initialValues}
          onSubmit={onSubmit}
          className="w-full flex flex-col gap-6"
        >
          <DialogHeader>
            <DialogTitle>{t("roles:add.title")}</DialogTitle>
          </DialogHeader>
          <div className="grid gap-4 py-4">
            <div className="flex items-center gap-4">
              <InputFormField label={t("fields:name")} name="roleName" />
            </div>
          </div>
          <DialogFooter>
            <SubmitButton label={t("fields:add")} />
          </DialogFooter>
        </Form>
      </DialogContent>
    </Dialog>
  )
}

export default AddRoleDialog
