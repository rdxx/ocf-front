import { toFormikValidationSchema } from "zod-formik-adapter"
import InputFormField from "../generics/form/InputFormField"
import SubmitButton from "../buttons/SubmitButton"
import useApiMutation from "@/hooks/useApiMutation"
import { useTranslation } from "next-i18next"
import { type User } from "@/types/courses"
import { Button } from "../generics/Button"
import Form from "../generics/form/Form"
import { useState } from "react"
import { z } from "zod"
import {
  Dialog,
  DialogTitle,
  DialogHeader,
  DialogFooter,
  DialogTrigger,
  DialogContent
} from "../generics/Dialog"

type AddOrganisationValues = z.infer<typeof validationSchema>

const validationSchema = z.object({
  email: z.string().email(),
  password: z.string().nonempty(),
  firstName: z.string().nonempty(),
  lastName: z.string().nonempty()
})

const initialValues = {
  email: "",
  password: "",
  firstName: "",
  lastName: ""
}

const AddUserDialog = () => {
  const { t } = useTranslation(["fields", "users"])
  const { mutate } = useApiMutation<AddOrganisationValues, User>(
    "post",
    "users/"
  )
  const [open, setOpen] = useState(false)

  const onSubmit = (values: AddOrganisationValues) => {
    mutate(values, {
      onSuccess: () => {
        setOpen(false)
      }
    })
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <Button variant="secondary">{t("users:add.title")}</Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <Form<AddOrganisationValues>
          validationSchema={toFormikValidationSchema(validationSchema)}
          initialValues={initialValues}
          onSubmit={onSubmit}
          className="w-full flex flex-col gap-6"
        >
          <DialogHeader>
            <DialogTitle>{t("users:add.title")}</DialogTitle>
          </DialogHeader>
          <div className="grid gap-4 py-4">
            <div className="flex flex-col items-center gap-4">
              <InputFormField
                label={t("fields:email")}
                name="email"
                type="email"
              />
              <InputFormField
                label={t("fields:password")}
                name="password"
                type="password"
              />
              <div className="flex gap-4 w-full">
                <InputFormField
                  label={t("fields:firstName")}
                  name="firstName"
                />
                <InputFormField label={t("fields:lastName")} name="lastName" />
              </div>
            </div>
          </div>
          <DialogFooter>
            <SubmitButton label={t("fields:add")} />
          </DialogFooter>
        </Form>
      </DialogContent>
    </Dialog>
  )
}

export default AddUserDialog
