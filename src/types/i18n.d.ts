import "i18next"

import type common from "public/locales/fr/common.json"
import type courses from "public/locales/fr/courses.json"
import type fields from "public/locales/fr/fields.json"
import type organisations from "public/locales/fr/organisations.json"
import type roles from "public/locales/fr/roles.json"
import type users from "public/locales/fr/users.json"
import type zod from "public/locales/fr/zod.json"

declare module "i18next" {
  interface CustomTypeOptions {
    defaultNS: "common"
    resources: {
      common: typeof common
      courses: typeof courses
      fields: typeof fields
      organisations: typeof organisations
      roles: typeof roles
      users: typeof users
      zod: typeof zod
    }
  }
}
