export type User = {
  id: string
  email: string
  firstName: string
  lastName: string
}

export type Section = {
  id: string
  createdAt: Date
  updatedAt: Date
  deletedAt: Date | null
  title: string
  number: number
  introduction: string
  conclusion: string
  fileName: string
}

export type Chapter = {
  id: string
  createdAt: Date
  updatedAt: Date
  deletedAt: Date | null
  title: string
  number: number
  footer: string
  introduction: string
  sections: Section[]
}

export type Course = {
  id: string
  name: string
  theme: string
  fileName: string
  format: number
  author: User
  title: string
  subtitle: string
  header: string
  footer: string
  logo: string
  description: string
  schedule: string
  prelude: string
  learningObjectives: string
  chapters: Chapter[]
}

export type ApiUser = {
  id: string
  email: string
  firstname: string
  lastname: string
}

export type ApiSection = {
  CreatedAt: string
  UpdatedAt: string
  DeletedAt: string | null
  ID: string
  FileName: string
  Title: string
  ParentChapterTitle: string
  Intro: string
  Conclusion: string
  Number: number
  pages: null
  HiddenPages: null
}

export type ApiCourse = {
  ID: string
  CourseID_str: string
  Name: string
  Theme: string
  Format: number
  Category: string
  Version: string
  Title: string
  Subtitle: string
  Header: string
  Footer: string
  Logo: string
  Description: string
  Schedule: string
  Prelude: string
  learning_objectives: string
  owner: ApiUser
  chapters: [
    {
      CreatedAt: string
      UpdatedAt: string
      DeletedAt: string | null
      ID: string
      Title: string
      Number: number
      Footer: string
      Introduction: string
      courses: null
      sections: ApiSection[]
    }
  ]
}
