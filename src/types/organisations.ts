export type Organisation = {
  id: string
  name: string
  groups: null
}
