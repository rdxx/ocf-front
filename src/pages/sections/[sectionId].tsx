import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import { type GetStaticProps, type GetStaticPaths } from "next"
import { type Section, type ApiSection } from "@/types/courses"
import { apiSectionToSection } from "@/utils/formatters"
import i18nConfig from "@/../next-i18next.config.cjs"
import useApiQuery from "@/hooks/useApiQuery"
import Page from "@/components/layout/Page"
import { useEffect, useState } from "react"
import { Marpit } from "@marp-team/marpit"
import { useRouter } from "next/router"
import { env } from "@/env.mjs"
import axios from "axios"

const CoursePage = () => {
  const { sectionId } = useRouter().query
  const { data: section, isFetching } = useApiQuery<Section, ApiSection>(
    "courses/fromSection/" + String(sectionId),
    {
      select: apiSectionToSection
    }
  )
  const [sectionContent, setSectionContent] = useState<string | null>(null)
  const marpit = new Marpit()

  useEffect(() => {
    const fetchSections = () => {
      if (section) {
        axios
          .get<string>(
            env.NEXT_PUBLIC_API_URL + "/courses_files/" + section.fileName
          )
          .then(({ data }) => {
            setSectionContent(data)
          })
          .catch((err) => {
            console.log(err)
          })
      }
    }

    void fetchSections()
  }, [section])

  if (!section || !sectionContent || isFetching) return <p>Loading...</p>

  const { html } = marpit.render(sectionContent ?? "")

  return (
    <Page>
      <div className="flex flex-col gap-8 w-full">{section.title}</div>
      <div dangerouslySetInnerHTML={{ __html: html }}></div>
    </Page>
  )
}

export default CoursePage

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? i18nConfig.i18n.defaultLocale, [
      "common"
    ]))
  }
})

export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: "blocking" //indicates the type of fallback
  }
}
