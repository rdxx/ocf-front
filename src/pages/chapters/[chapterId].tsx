import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import { type GetStaticProps, type GetStaticPaths } from "next"
import { type ApiCourse, type Course } from "@/types/courses"
import { apiCourseToCourse } from "@/utils/formatters"
import i18nConfig from "@/../next-i18next.config.cjs"
import useApiQuery from "@/hooks/useApiQuery"
import Page from "@/components/layout/Page"
import { useEffect, useState } from "react"
import { Marpit } from "@marp-team/marpit"
import { useRouter } from "next/router"
import { env } from "@/env.mjs"
import axios from "axios"

const CoursePage = () => {
  const { chapterId } = useRouter().query
  const { data: course, isFetching } = useApiQuery<Course, ApiCourse>(
    "courses/fromChapter/" + String(chapterId),
    { select: (data) => apiCourseToCourse(data) }
  )
  const chapter = course?.chapters?.find((chapter) => chapter.id === chapterId)
  const [sections, setSections] = useState<string[]>([])
  const marpit = new Marpit()
  const { html } = marpit.render(sections.join("\n"))

  useEffect(() => {
    const fetchSections = () => {
      if (chapter && sections.length === 0) {
        for (const section of chapter.sections) {
          axios
            .get<string>(
              env.NEXT_PUBLIC_API_URL + "/courses_files/" + section.fileName
            )
            .then((res) => {
              setSections((sections) => [...sections, res.data])
            })
            .catch((err) => {
              console.log(err)
            })
        }
      }
    }

    void fetchSections()
  }, [chapter, sections.length])

  if (!course || !chapter || isFetching) return <p>Loading...</p>

  return (
    <Page>
      <div className="flex flex-col gap-8 w-full">{chapter.title}</div>
      <div dangerouslySetInnerHTML={{ __html: html }}></div>
    </Page>
  )
}

export default CoursePage

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? i18nConfig.i18n.defaultLocale, [
      "common"
    ]))
  }
})

export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: "blocking" //indicates the type of fallback
  }
}
