import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import CoursesTable from "@/components/tables/CoursesTable"
import i18nConfig from "@/../next-i18next.config.cjs"
import LiveEditor from "@/components/ui/LiveEditor"
import { useTranslation } from "next-i18next"
import Page from "@/components/layout/Page"
import { type GetStaticProps } from "next"

const DashboardCourses = () => {
  const { t } = useTranslation("courses")

  return (
    <Page sidebar>
      <div className="flex flex-col gap-8">
        <h1 className="text-3xl font-bold">{t("title")}</h1>
        <CoursesTable />
        <LiveEditor />
      </div>
    </Page>
  )
}

export default DashboardCourses

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? i18nConfig.i18n.defaultLocale, [
      "common",
      "fields",
      "courses",
      "organisations",
      "roles",
      "users"
    ]))
  }
})
