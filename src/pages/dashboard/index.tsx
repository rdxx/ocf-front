import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import i18nConfig from "@/../next-i18next.config.cjs"
import Page from "@/components/layout/Page"
import { type GetStaticProps } from "next"

const DashboardHome = () => {
  return (
    <Page sidebar>
      <div>
        <h1 className="text-3xl font-bold">Home</h1>
      </div>
    </Page>
  )
}

export default DashboardHome

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? i18nConfig.i18n.defaultLocale, [
      "common",
      "courses",
      "organisations",
      "roles",
      "users"
    ]))
  }
})
