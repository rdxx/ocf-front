import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import { UsersTable } from "@/components/tables/UsersTable"
import i18nConfig from "@/../next-i18next.config.cjs"
import { useTranslation } from "next-i18next"
import Page from "@/components/layout/Page"
import { type GetStaticProps } from "next"

const DashboardUsers = () => {
  const { t } = useTranslation("users")

  return (
    <Page sidebar>
      <div className="flex flex-col gap-8">
        <h1 className="text-3xl font-bold">{t("title")}</h1>
        <UsersTable />
      </div>
    </Page>
  )
}

export default DashboardUsers

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? i18nConfig.i18n.defaultLocale, [
      "common",
      "fields",
      "courses",
      "organisations",
      "roles",
      "users"
    ]))
  }
})
