import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import { type GetStaticProps, type GetStaticPaths } from "next"
import { type ApiCourse, type Course } from "@/types/courses"
import CourseDetails from "@/components/ui/CourseDetails"
import ChaptersList from "@/components/ui/ChaptersList"
import { apiCourseToCourse } from "@/utils/formatters"
import i18nConfig from "@/../next-i18next.config.cjs"
import useApiQuery from "@/hooks/useApiQuery"
import Page from "@/components/layout/Page"
import { useRouter } from "next/router"

const CoursePage = () => {
  const { courseId } = useRouter().query
  const { data: course, isFetching } = useApiQuery<Course, ApiCourse>(
    "courses/" + String(courseId),
    {
      select: (data) => apiCourseToCourse(data)
    },
    { withCredentials: false }
  )

  if (!course || isFetching) return <p>Loading...</p>

  return (
    <Page>
      <div className="flex flex-col gap-8 w-full">
        <CourseDetails course={course} />
        <ChaptersList chapters={course.chapters} />
      </div>
    </Page>
  )
}

export default CoursePage

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? i18nConfig.i18n.defaultLocale, [
      "common",
      "courses"
    ]))
  }
})

export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: "blocking" //indicates the type of fallback
  }
}
