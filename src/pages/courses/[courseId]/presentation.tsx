import { type ApiCourse, type Course } from "@/types/courses"
import { apiCourseToCourse } from "@/utils/formatters"
import useApiQuery from "@/hooks/useApiQuery"
import { useRouter } from "next/router"

const CoursePresentationPage = () => {
  const { courseId } = useRouter().query
  const { data: course, isFetching } = useApiQuery<Course, ApiCourse>(
    "courses/" + String(courseId),
    {
      enabled: courseId !== undefined,
      select: (data) => apiCourseToCourse(data)
    }
  )

  if (!course || isFetching) {
    return <p>Loading...</p>
  }

  return (
    <iframe
      className="w-screen h-screen"
      src={`http://localhost:8000/files/${course.theme}/${course.fileName}`}
    />
  )
}

export default CoursePresentationPage
