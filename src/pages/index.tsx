import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import CoursesShowcase from "@/components/ui/CoursesShowcase"
import i18nConfig from "@/../next-i18next.config.cjs"
import { useTranslation } from "next-i18next"
import Page from "@/components/layout/Page"
import { type GetStaticProps } from "next"

export default function Home() {
  const { t } = useTranslation("courses")

  return (
    <Page>
      <div className="flex flex-col gap-6">
        <h2 className="text-3xl font-bold">{t("title")}</h2>
        <div className="flex gap-4">
          <CoursesShowcase />
          {/* {courses.length ? (
            <div>
              <p>azerty</p>
            </div>
          ) : (
            <p>{t("noCourses")}</p>
          )} */}
        </div>
      </div>
    </Page>
  )
}

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? i18nConfig.i18n.defaultLocale, [
      "common",
      "courses"
    ]))
  }
})
