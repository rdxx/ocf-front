import { QueryClient, QueryClientProvider } from "@tanstack/react-query"
import AuthProvider from "@/providers/AuthProvider"
import { appWithTranslation } from "next-i18next"
import { ThemeProvider } from "next-themes"
import type { AppProps } from "next/app"

import "@/styles/globals.css"

const queryClient = new QueryClient()

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <ThemeProvider attribute="class" defaultTheme="system" enableSystem>
      <QueryClientProvider client={queryClient}>
        <AuthProvider>
          <Component {...pageProps} />
        </AuthProvider>
      </QueryClientProvider>
    </ThemeProvider>
  )
}

export default appWithTranslation(App)
