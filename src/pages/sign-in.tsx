import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import InputFormField from "@/components/generics/form/InputFormField"
import { toFormikValidationSchema } from "zod-formik-adapter"
import i18nConfig from "@/../next-i18next.config.cjs"
import { Button } from "@/components/generics/Button"
import useApiMutation from "@/hooks/useApiMutation"
import { useAuth } from "@/providers/AuthProvider"
import Form from "@/components/generics/form/Form"
import { type LoginResponse } from "@/types/api"
import { useTranslation } from "next-i18next"
import Page from "@/components/layout/Page"
import { type GetStaticProps } from "next"
import { useRouter } from "next/router"
import { z } from "zod"

type SignInValues = z.infer<typeof validationSchema>

const validationSchema = z.object({
  email: z.string().email(),
  password: z.string().nonempty()
})

const initialValues = {
  email: "",
  password: ""
}

const SignIn = () => {
  const { mutate } = useApiMutation<SignInValues, LoginResponse>(
    "post",
    "login/"
  )
  const { t } = useTranslation(["common", "fields"])
  const router = useRouter()
  const { storeTokens } = useAuth()

  const onSubmit = (values: SignInValues) => {
    mutate(values, {
      onSuccess: (data) => {
        storeTokens(data.token, data.refresh_token)
        void router.replace("/")
      }
    })
  }

  return (
    <Page header={false} center>
      <div className="w-full flex gap-y-6 flex-col items-center">
        <h1 className="text-4xl font-semibold">{t("sign-in")}</h1>
        <div className="bg-background border p-8 flex rounded-md w-full">
          <Form<SignInValues>
            validationSchema={toFormikValidationSchema(validationSchema)}
            initialValues={initialValues}
            onSubmit={onSubmit}
            className="w-full flex flex-col gap-6"
          >
            <InputFormField
              name="email"
              label={t("fields:email")}
              type="email"
            />
            <InputFormField
              name="password"
              label={t("fields:password")}
              type="password"
            />
            <Button type="submit">{t("sign-in")}</Button>
          </Form>
        </div>
      </div>
    </Page>
  )
}

export default SignIn

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale ?? i18nConfig.i18n.defaultLocale, [
      "common",
      "fields"
    ]))
  }
})
